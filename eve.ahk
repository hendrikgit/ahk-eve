#Include %A_ScriptDir%\OnWin.ahk

Menu, Tray, Icon, %A_ScriptDir%\EveOnline.ico
Menu, Tray, Tip, EVE Mover

global tooltipNr := 0

global eveClass := "ahk_class triuiScreen"
global eveclientsExclude := "EVE - Hendrik Tiberius"
GroupAdd, eveclients, %eveClass%,,, %eveclientsExclude%
GroupAdd, browser, ahk_exe chrome.exe
GroupAdd, browser, EVE - Hendrik Tiberius

global windowPosition := {"EVE - Hendrik Tiberius": "Left", "EVE - Kira Tiberius": "Right", "EVE - Thera Tiberius": "Right"}
global windowPositionOnly := {"EVE - Hendrik Tiberius": "Right"}

for window in windowPosition
{
    OnWin("Exist", window, "OnExist")
}

OnExist(this)
{
    window := this.Window
    position := windowPosition[window]
    positionOnly := windowPositionOnly[window]
    WinGet, count, count, %eveClass%
    if (count = 1 && positionOnly != "")
    {
        position := positionOnly
    }
    MoveWindow(window, position)
    CheckNoLongerOnly()
    OnWin("NotExist", window, "OnNotExist")
}

MoveWindow(window, position)
{
    ;DisplayTooltip("Moving " window " to " position)
    if (position = "Left")
    {
        WinMove, %window%,, 0, 0
    }
    if (position = "Right")
    {
        WinMove, %window%,, 1920, 0
    }
}

CheckOnly()
{
    WinGet, count, count, %eveClass%
    if (count = 1)
    {
        WinGetTitle, windowOnly, %eveClass%
        positionOnly := windowPositionOnly[windowOnly]
        if (positionOnly != "")
        {
            MoveWindow(windowOnly, positionOnly)
        }
    }
}

CheckNoLongerOnly()
{
    WinGet, list, list, %eveClass%
    if (list > 1)
    {
        Loop, %list%
        {
            winID := list%A_Index%
            WinGetTitle window, ahk_id %winID%
            position := windowPosition[window]
            positionOnly := windowPositionOnly[window]
            if (positionOnly != "" && position != "")
            {
                MoveWindow(window, position)
            }
        }
    }
}

OnNotExist(this)
{
    window := this.Window    
    OnWin("Exist", window, "OnExist")
    CheckOnly()
}

DisplayTooltip(text)
{
    if (++tooltipNr > 20)
    {
        tooltipNr := 1
    }
    ToolTip, %text%,, (tooltipNr-1)*20, tooltipNr
    SetTimer, RemoveToolTips, -5000 ; negative time means run only once, if the time is not up the timer will be refreshed
    return

    RemoveToolTips:
    ;SetTimer, RemoveToolTip, Off
    Loop, %tooltipNr% ; remove all tooltips with number lower or equal to last displayed tooltip
    {
        ToolTip,,,, %A_Index%
    }
    return
}

return ; End of autoexecute section

capslock::
    GroupActivate, eveclients
return

; shift + capslock
+capslock::
    GroupActivate, browser
return

;control + capslock
^capslock::
    WinActivate, pyfa
return

capslock & 1::
    IfWinExist, EVE - Hendrik Tiberius
    WinActivate
return

capslock & 2::
    IfWinExist, EVE - Kira Tiberius
    WinActivate
return

capslock & 3::
    IfWinExist, EVE - Thera Tiberius
    WinActivate
return